defmodule IboardPhoenix.PageController do
  alias IBoard.Repository.Boards
  use IboardPhoenix.Web, :controller
  require FakeData

  plug :action
  plug :accepts, ["json","html"]


  def index(conn, params ) do
    conn |> load_index |> render_index(params["format"])
  end

  def show(conn, params) do
    conn |> load_index |> render_board( params["format"], params["id"] )
  end

  # RENDER FUNCTIONS

  defp render_board(conn, format, board_id) when( format == "html" ) do
    render conn, "show.html", %{board: get_board(board_id)}
  end

  defp render_board(conn, format, board_id) when( format == "json" )  do
    conn
      |> put_resp_content_type("Application/json")
      |> put_resp_header("Access-Control-Allow-Origin", "http://static.iboard.cc")
      |> json %{status: "ok", data: get_board(board_id)}
  end

  defp render_index(conn, format) when(format == "html") do
    conn
      |> render "index.html", %{boards: Boards.index}
  end

  defp render_index(conn, format) when(format == "json") do
    conn
      |> put_resp_content_type("Application/json")
      |> put_resp_header("Access-Control-Allow-Origin", "http://static.iboard.cc")
      |> json %{ status: "ok", data: Boards.index }
  end

  # DATA FUNCTIONS

  defp load_index conn do
    Boards.drop!
    FakeData.load_fixtures(:boards, data_file)
    conn
  end


  defp data_file do
    [path: file] = IboardPhoenix.Endpoint.config( :data, "/tmp/data.#{Mix.env}" )
    file
  end

  defp get_board(board_id) do
    Boards.find String.to_integer(board_id)
  end

end
