defmodule IboardPhoenix.PageView do
  use IboardPhoenix.Web, :view

  def board_list boards do
  end

  def render_topic_items topic do
    try do
      Enum.map topic.items, fn(item) ->
        render("board_topic_item.html", item: item )
      end
    catch
      :error, value  ->
        case value do
          %KeyError{key: :items} -> "No Items"
          _ -> "Unexpected error #{inspect value}"
        end
    end
  end
end
