defmodule IboardPhoenix.Router do
  use IboardPhoenix.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", IboardPhoenix do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/boards", PageController, :index
    get "/boards/:id", PageController, :show
  end

  scope "/api", IboardPhoenix do
    pipe_through :api # Use the default browser stack

    get "/", PageController, :index
    get "/boards", PageController, :index
    get "/boards/:id", PageController, :show
  end
end
