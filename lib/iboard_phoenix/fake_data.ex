defmodule FakeData do

  def load_fixtures(:boards, file) do
    File.read!(file)
    |> Poison.Parser.parse!(keys: :atoms!)
    |> Enum.reverse |> Enum.map fn(board) ->
      Map.merge(%IBoard.Entities.Board{},board)
      |> IBoard.Repository.Boards.add
    end
  end
end
