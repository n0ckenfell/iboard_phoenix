defmodule IboardPhoenix.PageControllerTest do
  use IboardPhoenix.ConnCase

  # BOARD INDEX

  test "GET /" do
    conn = get conn(), "/"
    assert html_response(conn, 200) =~ "board-list"
  end

  test "GET /boards" do
    conn = get conn(), "/"
    assert html_response(conn, 200) =~ "board-list"
  end

  test "GET /api/boards" do
    conn      = get conn(), "/api/boards"
    [board|_] = json_response(conn,200)["data"]

    assert board["name"] == "Welcome Board"
  end

  # SHOW BOARD

  test "GET /boards/:id" do
    conn = get conn(), "/boards/1"
    assert html_response(conn, 200) =~ "Welcome Board"
  end

  test "GET /api/boards/:id" do
    conn     = get conn(), "/api/boards/1"
    board = json_response(conn,200)["data"]

    assert board["name"] == "Welcome Board"
  end


end
